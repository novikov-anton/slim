<?php

require 'vendor/autoload.php';

$app = new Slim\App();

function getConnection() {
    $dbhost="127.0.0.1";
    $dbuser="root";
    $dbpass="root";
    $dbname="slim";
    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
}

// curl -H "Accept: application/json" -H "Content-Type: application/json" -d '{"name":"Anton","email":"test@gmail.com", "pass": "test", "age":"1986-02-04"}' -X POST http://localhost:8888/api/signup
$app->post('/api/signup', function ($request, $response, $args) {

    $body = $request->getParsedBody();

    $responseWithJson = $response->withJson([], 400);

    $sql = "INSERT INTO users (`name`,`email`,`pass`,`age`) VALUES (:name, :email, :pass, :age);";
    try {

        $db = getConnection();
        $stmt = $db->prepare($sql);

        try {
            $db->beginTransaction();

            $pass = md5($body['pass']);
            $name = $body['name'];

            $stmt->bindParam("name", $name);
            $stmt->bindParam("email", $body['email']);
            $stmt->bindParam("pass", $pass);
            $stmt->bindParam("age", $body['age']);

            if ( false === $stmt->execute() ) {
                throw new PDOException("Can't create user");
            }

            $userId = $db->lastInsertId();
            $accessToken = sha1($userId.time().$pass);

            $sql = "INSERT INTO access (`user_id`,`access_token`,`date`) VALUES (:user_id, :access_token, :date);";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("user_id", $userId);
            $stmt->bindParam("access_token", $accessToken);
            $stmt->bindParam("date", date('Y-m-d H:i:s'));

            if ( false === $stmt->execute() ) {
                throw new PDOException("Can't create user");
            }

            $db->commit();
            $db = null;

            $responseWithJson = $response->withJson(['access_token' => $accessToken], 200);


        } catch (PDOException $e) {
            $db->rollBack();
            $responseWithJson = $response->withJson(['error' => $e->getMessage()], 500);
        }

    } catch(PDOException $e) {
        $responseWithJson = $response->withJson(['error' => $e->getMessage()], 500);
    }

    return $responseWithJson;
});

// curl -H "Accept: application/json" -H "Content-Type: application/json" -d '{"email":"test@gmail.com", "pass": "test"}' -X POST http://localhost:8888/api/login
$app->post('/api/login', function ($request, $response, $args) {

    $body = $request->getParsedBody();

    $responseWithJson = $response->withJson([], 401);

    $pass = md5($body['pass']);
    $email = $body['email'];

    $sql = "SELECT * FROM users WHERE email=:email AND pass=:pass;";
    try {

        $db = getConnection();
        $stmt = $db->prepare($sql);

        $stmt->bindParam("pass", $pass);
        $stmt->bindParam("email", $email);

        if ( false === $stmt->execute() ) {
            throw new PDOException("DB error", 500);
        }

        if ( false === ($row = $stmt->fetch()) ) {
            throw new Exception("User not found", 401);
        }

        $userId = $row['id'];
        $sql = "SELECT * FROM access WHERE user_id=:user_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $userId);

        if ( false === $stmt->execute() ) {
            throw new PDOException("DB error", 500);
        }

        if ( false === ($row = $stmt->fetch()) )
        {
            $accessToken = sha1($userId.time().$pass);

            $sql = "INSERT INTO access (`user_id`,`access_token`,`date`) VALUES (:user_id, :access_token, :date);";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("user_id", $userId);
            $stmt->bindParam("access_token", $accessToken);
            $stmt->bindParam("date", date('Y-m-d H:i:s'));

            if ( false === $stmt->execute() ) {
                throw new Exception("Auth error", 401);
            }
        }
        else {
            $accessToken = $row['access_token'];
        }

        $responseWithJson = $response->withJson(['access_token' => $accessToken], 200);

    } catch(Exception $e) {
        $responseWithJson = $response->withJson(['error' => $e->getMessage()], $e->getCode());
    }

    return $responseWithJson;
});

// curl -H "Accept: application/json" -H "Content-Type: application/json" -d '{"access_token":"5f0b3b7943b150829a8d57e9e29f91bfc2716c06"}' -X GET http://localhost:8888/api/profile
$app->get('/api/profile', function ($request, $response, $args) {

    $body = $request->getParsedBody();
    $accessToken = $body['access_token'];

    $sql = "SELECT * FROM access WHERE access_token=:access_token";
    try {

        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("access_token", $accessToken);

        if ( false === $stmt->execute() ) {
            throw new Exception("DB error", 500);
        }

        if ( false === ($row = $stmt->fetch()) ) {
            throw new Exception("Auth error", 401);
        }

        $userId = $row['user_id'];
        $sql = "SELECT * FROM users WHERE id=:user_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $userId);

        if ( false === $stmt->execute() ) {
            throw new Exception("DB error", 500);
        }

        if ( false === ($row = $stmt->fetch()) ) {
            throw new Exception("User not found", 401);
        }

        $responseWithJson = $response->withJson(['name'=>$row['name'],'email'=>$row['email'],'age'=>$row['age']], 200);

    } catch(Exception $e) {
        $responseWithJson = $response->withJson(['error' => $e->getMessage()], $e->getCode());
    }

    return $responseWithJson;
});

$app->run();